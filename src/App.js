import logo from "./logo.svg";
import "./App.css";
import Title from "./components/Tittle";
import Card from "./components/Card";
import LoginForm from "./components/LoginForm";

function App() {
  return (
    <div className="App">
      <Title>Testing connection to git using wsl</Title>
      <Card>
        <LoginForm />
      </Card>
    </div>
  );
}

export default App;
