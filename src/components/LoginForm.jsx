import "./Card.css";
import { useForm } from "react-hook-form";
const LoginForm = () => {
  const submit = (data, e) => {
    e.preventDefault();
    console.log(data);
  };
  const { register, handleSubmit } = useForm();

  return (
    <form onSubmit={handleSubmit(submit)}>
      <div className="form-control">
        <label htmlFor="username">Username:</label>
        <input
          type="text"
          name="username"
          id="username"
          {...register("username", { required: true })}
        />
      </div>
      <div className="form-control">
        <label htmlFor="password">Password:</label>
        <input
          name="password"
          id="password"
          type="password"
          {...register("password", { required: true })}
        />
      </div>

      <input type="submit" value="Log In" />
    </form>
  );
};

export default LoginForm;
